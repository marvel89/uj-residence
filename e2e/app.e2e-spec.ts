import { UjResidencePage } from './app.po';

describe('uj-residence App', () => {
  let page: UjResidencePage;

  beforeEach(() => {
    page = new UjResidencePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
